﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimezoneConverter.Model;

namespace TimezoneConverter.Data
{
    public class Person
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public String Name { get; set; }
        public int Age { get; set; }
    }

    public class FakeService
    {
        static string path;
        static SQLite.Net.SQLiteConnection conn = null;

        private static void SetupDB()
        {
            path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path,
            "db.sqlite");
            conn = new SQLite.Net.SQLiteConnection(new
               SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path);
            conn.CreateTable<Person>();
        }

        public static String Name = "Fake Data Service.";

        public static List<Person> GetPeople()
        {
            if (conn == null)
            {
                SetupDB();
            }
            var table = conn.Table<Person>();
            var Persons = new List<Person>();
            foreach (var message in table)
            {
                var per = new Person();
                per.id = message.id;
                per.Name = message.Name;
                per.Age = message.Age;
                Persons.Add(per);
            }

            return Persons;
        }

        public static void Write(Person person)
        {
            if (conn == null)
            {
                SetupDB();
            } 
            conn.Insert(person);
        }

        public static void Delete(Person person)
        {
            if (conn == null)
            {
                SetupDB();
            }
            conn.Delete(person);
        }

        public static void Update(Person person)
        {
            if (conn == null)
            {
                SetupDB();
            }
            conn.Update(person);
        }
    }

}
